<?php

/* AppBundle:User:login.html.twig */
class __TwigTemplate_b4fc7990debbff13fec168e1a550be497e0b67dbda9ac48fa9e7541fec5be27d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle:Layouts:base.html.twig", "AppBundle:User:login.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle:Layouts:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8501fd1ce8bf170280f06cac75aeb3b9bd0e99175f4f75eddf529ffe2dda8e35 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8501fd1ce8bf170280f06cac75aeb3b9bd0e99175f4f75eddf529ffe2dda8e35->enter($__internal_8501fd1ce8bf170280f06cac75aeb3b9bd0e99175f4f75eddf529ffe2dda8e35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8501fd1ce8bf170280f06cac75aeb3b9bd0e99175f4f75eddf529ffe2dda8e35->leave($__internal_8501fd1ce8bf170280f06cac75aeb3b9bd0e99175f4f75eddf529ffe2dda8e35_prof);

    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        $__internal_6d922395d7bf7a18a48c4595fe1153e659746fe1510e57e712c28b872362a282 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d922395d7bf7a18a48c4595fe1153e659746fe1510e57e712c28b872362a282->enter($__internal_6d922395d7bf7a18a48c4595fe1153e659746fe1510e57e712c28b872362a282_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "       <div class=\"col-lg-4 box-form\">
           
           <h2>Identificarse</h2>
           <hr/>
           
           ";
        // line 10
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 11
            echo "               ";
            echo twig_escape_filter($this->env, twig_var_dump($this->env, $context, $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())), "html", null, true);
            echo "
           ";
        }
        // line 13
        echo "           
           <form action=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login_check");
        echo "\" method=\"POST\">
               <label>
                   Email
               </label>
               <input type=\"email\" id=\"username\" name=\"_username\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" class=\"form-control\"/>
               <label>
                   Contrasena
               </label>
               <input type=\"password\" id=\"password\" name=\"_password\" value=\"\" class=\"form-control\"/>
               <input type=\"submit\" value=\"Entrar\" class=\"btn-success\"/>
               <input type=\"hidden\" name=\"_target_path\" value=\"/home\"/>
           </form>
           
           
       </div>
    ";
        
        $__internal_6d922395d7bf7a18a48c4595fe1153e659746fe1510e57e712c28b872362a282->leave($__internal_6d922395d7bf7a18a48c4595fe1153e659746fe1510e57e712c28b872362a282_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 18,  58 => 14,  55 => 13,  49 => 11,  47 => 10,  40 => 5,  34 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("    {% extends \"AppBundle:Layouts:base.html.twig\" %}

    
    {% block content %}
       <div class=\"col-lg-4 box-form\">
           
           <h2>Identificarse</h2>
           <hr/>
           
           {% if is_granted('ROLE_USER') %}
               {{ dump(app.user) }}
           {% endif %}
           
           <form action=\"{{path('login_check')}}\" method=\"POST\">
               <label>
                   Email
               </label>
               <input type=\"email\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" class=\"form-control\"/>
               <label>
                   Contrasena
               </label>
               <input type=\"password\" id=\"password\" name=\"_password\" value=\"\" class=\"form-control\"/>
               <input type=\"submit\" value=\"Entrar\" class=\"btn-success\"/>
               <input type=\"hidden\" name=\"_target_path\" value=\"/home\"/>
           </form>
           
           
       </div>
    {% endblock %}
    ", "AppBundle:User:login.html.twig", "/home/ubuntu/workspace/src/AppBundle/Resources/views/User/login.html.twig");
    }
}
