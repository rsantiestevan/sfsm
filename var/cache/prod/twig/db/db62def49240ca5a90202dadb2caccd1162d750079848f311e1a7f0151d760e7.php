<?php

/* AppBundle:Layouts:base.html.twig */
class __TwigTemplate_119e1d7421c92d533d23aa9927199c99dae9f87d2c48ebc085cfcae28e43e7b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4fab13f97bcad150b8e34e277b2959ce44d67b6fcaadaeb0b8e3a163b029e541 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fab13f97bcad150b8e34e277b2959ce44d67b6fcaadaeb0b8e3a163b029e541->enter($__internal_4fab13f97bcad150b8e34e277b2959ce44d67b6fcaadaeb0b8e3a163b029e541_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Layouts:base.html.twig"));

        // line 1
        echo "<!DOCTYPE HTML>
<html lang=\"es\">
    <head>
        <meta charset=\"utf-8\" />
        <title>
            ";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        // line 7
        echo "        </title>
        
        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 15
        echo "        
        ";
        // line 16
        $this->displayBlock('javascripts', $context, $blocks);
        // line 23
        echo "    </head>
    <body>
        <header>
            
            <nav class=\"navbar navbar-inverse\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapse\" data-togle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Navegacion</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"navbar-brand\" href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_homepage");
        echo "\">
                            <span class=\"glyphicon glyphicon-cloud\" aria-hidden=\"true\"></span>
                            Network
                        </a>
                    </div>

                    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                        ";
        // line 43
        if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()) == null)) {
            // line 44
            echo "                            <ul class=\"nav navbar-nav\">
                                <li>
                                    <a href=\"";
            // line 46
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
            echo "\">
                                        <span class=\"glyphicon glyphicon-log-in\" aria-hidden=\"true\"></span>>&nbsp;
                                        Entrar
                                    </a>
                                </li>
                                <li>
                                    <a href=\"";
            // line 52
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("register");
            echo "\">
                                        <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>&nbsp;
                                        Registro
                                    </a>
                                </li>
                            </ul>
                        ";
        }
        // line 59
        echo "                        
                        ";
        // line 60
        if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()) != null)) {
            // line 61
            echo "                            <ul class=\"nav navbar-nav navbar-right\">
                                <li class=\"dropdown\">
                                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                        <div class=\"avatar\">
                                            ";
            // line 65
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "image", array()) == null)) {
                // line 66
                echo "                                                <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/assets/images/default.png"), "html", null, true);
                echo "\">
                                            ";
            } else {
                // line 68
                echo "                                                <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/users/" . $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "image", array()))), "html", null, true);
                echo "\">
                                            ";
            }
            // line 70
            echo "                                        </div>&nbsp;
                                        ";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "surname", array()), "html", null, true);
            echo "
                                        <span class=\"caret\"></span>
                                    </a>
                                    <ul class=\"dropdown-menu\">
                                        <li>
                                            <a href=\"";
            // line 76
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_edit");
            echo "\">
                                                <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span> &nbsp;
                                                Mis Datos
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"\">
                                                <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span> &nbsp;
                                                Mi Perfil
                                            </a>
                                        </li>
                                        <li role=\"serparator\" class=\"divider\"></li>
                                        <li>
                                            <a href=\"";
            // line 89
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("logout");
            echo "\">
                                                <span class=\"glyphicon glyphicon-log-out\" aria-hidden=\"true\"></span> &nbsp;
                                                Salir
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"\">
                                                <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> &nbsp;
                                                Mis Datos
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        ";
        }
        // line 104
        echo "                        
                    </div>
                    
                </div>                
            </nav>
        </header>
        <section id=\"content\">
            <div class=\"container\">
                <div class=\"col-lg-11\">
                    ";
        // line 113
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array(), "method"), "get", array(0 => "status"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 114
            echo "                        <div class=\"alert alert-success\">
                            ";
            // line 115
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 118
        echo "                </div>
            </div>
            <div class=\"clearfix\"></div></div>
            ";
        // line 121
        $this->displayBlock('content', $context, $blocks);
        // line 124
        echo "        </section>
        <footer class=\"col-lg-12\">
            <hr/>
            <div class=\"container\">
            <p class=\"text-muted\">Desarrollar una red social con Symfony3 - Richard Santiestevan</p>
            </div>
        </footer>
    </body>
    
</html>";
        
        $__internal_4fab13f97bcad150b8e34e277b2959ce44d67b6fcaadaeb0b8e3a163b029e541->leave($__internal_4fab13f97bcad150b8e34e277b2959ce44d67b6fcaadaeb0b8e3a163b029e541_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_810b368688d6fe547b75d10528fc60997f0e64fc5c6bb76ae6c565db230a92ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_810b368688d6fe547b75d10528fc60997f0e64fc5c6bb76ae6c565db230a92ec->enter($__internal_810b368688d6fe547b75d10528fc60997f0e64fc5c6bb76ae6c565db230a92ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " Network ";
        
        $__internal_810b368688d6fe547b75d10528fc60997f0e64fc5c6bb76ae6c565db230a92ec->leave($__internal_810b368688d6fe547b75d10528fc60997f0e64fc5c6bb76ae6c565db230a92ec_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_41acc3ac82c0efef8308c0842d2f7bbbde03eb2d39349750d54ee3eef267da0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41acc3ac82c0efef8308c0842d2f7bbbde03eb2d39349750d54ee3eef267da0d->enter($__internal_41acc3ac82c0efef8308c0842d2f7bbbde03eb2d39349750d54ee3eef267da0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "            <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/assets/css/bootstrap.cosmo.min.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/assets/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
            
        ";
        
        $__internal_41acc3ac82c0efef8308c0842d2f7bbbde03eb2d39349750d54ee3eef267da0d->leave($__internal_41acc3ac82c0efef8308c0842d2f7bbbde03eb2d39349750d54ee3eef267da0d_prof);

    }

    // line 16
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_33ec3c3f99485205919fcd1303a39cb716e1ce42ec98f7c5f9fa7383a355a449 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33ec3c3f99485205919fcd1303a39cb716e1ce42ec98f7c5f9fa7383a355a449->enter($__internal_33ec3c3f99485205919fcd1303a39cb716e1ce42ec98f7c5f9fa7383a355a449_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 17
        echo "            <script type=\"text/javascript\">
                var URL = \"https://";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "host", array()), "html", null, true);
        echo "\";
            </script>
            <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/assets/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_33ec3c3f99485205919fcd1303a39cb716e1ce42ec98f7c5f9fa7383a355a449->leave($__internal_33ec3c3f99485205919fcd1303a39cb716e1ce42ec98f7c5f9fa7383a355a449_prof);

    }

    // line 121
    public function block_content($context, array $blocks = array())
    {
        $__internal_eb09eccd3beb91693121a6fe9eb355a9f5fd054ec55f70e31c6c4a14147fa7e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb09eccd3beb91693121a6fe9eb355a9f5fd054ec55f70e31c6c4a14147fa7e2->enter($__internal_eb09eccd3beb91693121a6fe9eb355a9f5fd054ec55f70e31c6c4a14147fa7e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 122
        echo "                content default
            ";
        
        $__internal_eb09eccd3beb91693121a6fe9eb355a9f5fd054ec55f70e31c6c4a14147fa7e2->leave($__internal_eb09eccd3beb91693121a6fe9eb355a9f5fd054ec55f70e31c6c4a14147fa7e2_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Layouts:base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  288 => 122,  282 => 121,  273 => 21,  269 => 20,  264 => 18,  261 => 17,  255 => 16,  245 => 12,  241 => 11,  236 => 10,  230 => 9,  218 => 6,  202 => 124,  200 => 121,  195 => 118,  186 => 115,  183 => 114,  179 => 113,  168 => 104,  150 => 89,  134 => 76,  124 => 71,  121 => 70,  115 => 68,  109 => 66,  107 => 65,  101 => 61,  99 => 60,  96 => 59,  86 => 52,  77 => 46,  73 => 44,  71 => 43,  61 => 36,  46 => 23,  44 => 16,  41 => 15,  39 => 9,  35 => 7,  33 => 6,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE HTML>
<html lang=\"es\">
    <head>
        <meta charset=\"utf-8\" />
        <title>
            {% block title %} Network {% endblock %}
        </title>
        
        {% block stylesheets %}
            <link href=\"{{ asset('assets/assets/bootstrap/css/bootstrap.min.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"{{ asset('assets/assets/css/bootstrap.cosmo.min.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"{{ asset('assets/assets/css/style.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
            
        {% endblock %}
        
        {% block javascripts %}
            <script type=\"text/javascript\">
                var URL = \"https://{{ app.request.host }}\";
            </script>
            <script src=\"{{ asset('assets/assets/js/jquery.min.js') }}\"></script>
            <script src=\"{{ asset('assets/assets/bootstrap/js/bootstrap.min.js') }}\"></script>
        {% endblock %}
    </head>
    <body>
        <header>
            
            <nav class=\"navbar navbar-inverse\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapse\" data-togle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Navegacion</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"navbar-brand\" href=\"{{path(\"app_homepage\")}}\">
                            <span class=\"glyphicon glyphicon-cloud\" aria-hidden=\"true\"></span>
                            Network
                        </a>
                    </div>

                    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                        {% if app.user == null %}
                            <ul class=\"nav navbar-nav\">
                                <li>
                                    <a href=\"{{ path(\"login\") }}\">
                                        <span class=\"glyphicon glyphicon-log-in\" aria-hidden=\"true\"></span>>&nbsp;
                                        Entrar
                                    </a>
                                </li>
                                <li>
                                    <a href=\"{{ path(\"register\") }}\">
                                        <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>&nbsp;
                                        Registro
                                    </a>
                                </li>
                            </ul>
                        {% endif %}
                        
                        {% if app.user != null %}
                            <ul class=\"nav navbar-nav navbar-right\">
                                <li class=\"dropdown\">
                                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                        <div class=\"avatar\">
                                            {%if app.user.image == null %}
                                                <img src=\"{{ asset('assets/assets/images/default.png') }}\">
                                            {%else%}
                                                <img src=\"{{ asset('uploads/users/'~app.user.image) }}\">
                                            {%endif%}
                                        </div>&nbsp;
                                        {{ app.user.name }} {{ app.user.surname }}
                                        <span class=\"caret\"></span>
                                    </a>
                                    <ul class=\"dropdown-menu\">
                                        <li>
                                            <a href=\"{{ path(\"user_edit\") }}\">
                                                <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span> &nbsp;
                                                Mis Datos
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"\">
                                                <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span> &nbsp;
                                                Mi Perfil
                                            </a>
                                        </li>
                                        <li role=\"serparator\" class=\"divider\"></li>
                                        <li>
                                            <a href=\"{{path(\"logout\") }}\">
                                                <span class=\"glyphicon glyphicon-log-out\" aria-hidden=\"true\"></span> &nbsp;
                                                Salir
                                            </a>
                                        </li>
                                        <li>
                                            <a href=\"\">
                                                <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> &nbsp;
                                                Mis Datos
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        {% endif %}
                        
                    </div>
                    
                </div>                
            </nav>
        </header>
        <section id=\"content\">
            <div class=\"container\">
                <div class=\"col-lg-11\">
                    {% for message in app.session.flashbag().get('status') %}
                        <div class=\"alert alert-success\">
                            {{message}}
                        </div>
                    {% endfor %}
                </div>
            </div>
            <div class=\"clearfix\"></div></div>
            {% block content %}
                content default
            {% endblock %}
        </section>
        <footer class=\"col-lg-12\">
            <hr/>
            <div class=\"container\">
            <p class=\"text-muted\">Desarrollar una red social con Symfony3 - Richard Santiestevan</p>
            </div>
        </footer>
    </body>
    
</html>", "AppBundle:Layouts:base.html.twig", "/home/ubuntu/workspace/src/AppBundle/Resources/views/Layouts/base.html.twig");
    }
}
