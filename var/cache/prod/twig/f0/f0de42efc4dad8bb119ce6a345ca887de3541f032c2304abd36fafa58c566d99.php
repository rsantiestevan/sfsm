<?php

/* AppBundle:User:register.html.twig */
class __TwigTemplate_76fc04b3b6429dc5bc98eba8deef238eb5c3d544615c83f5db904ddb0890b8ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle:Layouts:base.html.twig", "AppBundle:User:register.html.twig", 1);
        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle:Layouts:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d88bc1269420ca197b7fb862e09874defbfe3940bd527fd473a6172c65d295c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d88bc1269420ca197b7fb862e09874defbfe3940bd527fd473a6172c65d295c1->enter($__internal_d88bc1269420ca197b7fb862e09874defbfe3940bd527fd473a6172c65d295c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d88bc1269420ca197b7fb862e09874defbfe3940bd527fd473a6172c65d295c1->leave($__internal_d88bc1269420ca197b7fb862e09874defbfe3940bd527fd473a6172c65d295c1_prof);

    }

    // line 2
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_d8a5099f39e38b92c29bf2d27e73503ed7bbede52364f07b57ba482b89b83fdd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8a5099f39e38b92c29bf2d27e73503ed7bbede52364f07b57ba482b89b83fdd->enter($__internal_d8a5099f39e38b92c29bf2d27e73503ed7bbede52364f07b57ba482b89b83fdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 3
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
        <script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/assets/js/custom/nick-test.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_d8a5099f39e38b92c29bf2d27e73503ed7bbede52364f07b57ba482b89b83fdd->leave($__internal_d8a5099f39e38b92c29bf2d27e73503ed7bbede52364f07b57ba482b89b83fdd_prof);

    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        $__internal_bc0cb195a21795084e7ead62ad9d322dae37462a1d4dd0404022e0d27c398d52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc0cb195a21795084e7ead62ad9d322dae37462a1d4dd0404022e0d27c398d52->enter($__internal_bc0cb195a21795084e7ead62ad9d322dae37462a1d4dd0404022e0d27c398d52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 8
        echo "       <div class=\"col-lg-8 box-form\">
           
           <h2>Register</h2>
           <hr/>
           
           ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => "", "method" => "POST"));
        echo "
           ";
        // line 14
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
           ";
        // line 15
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
           
       </div>
    ";
        
        $__internal_bc0cb195a21795084e7ead62ad9d322dae37462a1d4dd0404022e0d27c398d52->leave($__internal_bc0cb195a21795084e7ead62ad9d322dae37462a1d4dd0404022e0d27c398d52_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 15,  72 => 14,  68 => 13,  61 => 8,  55 => 7,  46 => 4,  41 => 3,  35 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("    {% extends \"AppBundle:Layouts:base.html.twig\" %}
    {% block javascripts %}
        {{ parent() }}
        <script src=\"{{ asset('assets/assets/js/custom/nick-test.js') }}\"></script>
    {% endblock %}

    {% block content %}
       <div class=\"col-lg-8 box-form\">
           
           <h2>Register</h2>
           <hr/>
           
           {{form_start(form, {'action':'', 'method':'POST'})}}
           {{form_errors(form)}}
           {{form_end(form)}}
           
       </div>
    {% endblock %}", "AppBundle:User:register.html.twig", "/home/ubuntu/workspace/src/AppBundle/Resources/views/User/register.html.twig");
    }
}
