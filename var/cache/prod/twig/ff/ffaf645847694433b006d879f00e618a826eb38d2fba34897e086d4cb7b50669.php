<?php

/* AppBundle:User:edit_user.html.twig */
class __TwigTemplate_973cc3b21f166c366e21106b98ccb6c87df447b11035c0a532f53914c8317315 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle:Layouts:base.html.twig", "AppBundle:User:edit_user.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle:Layouts:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a20d0284382b8922decfe7f585722728af5500e6dea0bc74580c21750676ee4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a20d0284382b8922decfe7f585722728af5500e6dea0bc74580c21750676ee4->enter($__internal_1a20d0284382b8922decfe7f585722728af5500e6dea0bc74580c21750676ee4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:edit_user.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1a20d0284382b8922decfe7f585722728af5500e6dea0bc74580c21750676ee4->leave($__internal_1a20d0284382b8922decfe7f585722728af5500e6dea0bc74580c21750676ee4_prof);

    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        $__internal_501a09f75ed807c14a691f7c6bd3a4ead86551e43a77ee847434c3da1a09b5b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_501a09f75ed807c14a691f7c6bd3a4ead86551e43a77ee847434c3da1a09b5b3->enter($__internal_501a09f75ed807c14a691f7c6bd3a4ead86551e43a77ee847434c3da1a09b5b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "       <div class=\"col-lg-8 box-form\">
           <h2>Mis datos</h2>
           <hr/>
           
           ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("enctype" => "multipart/form-data"));
        echo "
           
           ";
        // line 11
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
       </div>
    ";
        
        $__internal_501a09f75ed807c14a691f7c6bd3a4ead86551e43a77ee847434c3da1a09b5b3->leave($__internal_501a09f75ed807c14a691f7c6bd3a4ead86551e43a77ee847434c3da1a09b5b3_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:edit_user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 11,  46 => 9,  40 => 5,  34 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("    {% extends \"AppBundle:Layouts:base.html.twig\" %}


    {% block content %}
       <div class=\"col-lg-8 box-form\">
           <h2>Mis datos</h2>
           <hr/>
           
           {{form_start(form, {'enctype':'multipart/form-data'})}}
           
           {{form_end(form)}}
       </div>
    {% endblock %}", "AppBundle:User:edit_user.html.twig", "/home/ubuntu/workspace/src/AppBundle/Resources/views/User/edit_user.html.twig");
    }
}
